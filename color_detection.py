from scipy.io import loadmat
import numpy as np
import cv2

upp_red = np.array([80,80,255],dtype='uint8')
low_red = np.array([0,0,136],dtype='uint8')
upp_gre = np.array([120,177,120],dtype='uint8')
low_gre = np.array([42,130,55],dtype='uint8')
upp_blu = np.array([202,160,10],dtype='uint8')
low_blu = np.array([125,94,0],dtype='uint8')
boundaries = [(upp_red,low_red),(upp_gre,low_gre),(upp_blu,low_blu)]

file_name="2020-04-23-10-37-53.mat"
data = loadmat(file_name) # načtení dat
# print(data['image_rgb'])  # zobrazení jedné proměnné

# img = cv2.imread()
img=data['image_rgb']
print("Data type of img:")
type(img)
depth=data['image_depth']
# print(len(depth[0]))
screen_res = 480, 640
scale_width = screen_res[0] / img.shape[1]
scale_height = screen_res[1] / img.shape[0]
scale = min(scale_width, scale_height)
window_width = int(img.shape[1] * scale)
window_height = int(img.shape[0] * scale)
# print(img)
cv2.namedWindow('camera', cv2.WINDOW_NORMAL)
cv2.resizeWindow('camera', window_width, window_height)

# Write some Text
# print(type(img))
font = cv2.FONT_HERSHEY_SIMPLEX
bottomLeftCornerOfText = (0,50)
fontScale = 1
fontColor = (255,255,255)
lineType = 2

mouseX=0
mouseY=0

def RGB2HSV(rgb):
    HSV = cv2.cvtColor(rgb, cv2.COLOR_BGR2HSV)
    return HSV

def mouse_clicked(event, x, y, flags, param):
    global mouseX, mouseY
    if event == cv2.EVENT_LBUTTONDOWN:
        # cv2.circle(img, (x, y), 100, (255, 0, 0), -1)
        mouseX, mouseY = x, y

class Boundaries:
    def __init__(self, boundaries):
        self.boundaries = boundaries
    def adjust_bound(self, index, bgr):
        color = self.boundaries[index]
        for i in range(3):
            self.boundaries[index][0][i] = (color[0][i] + bgr[i])%256
            self.boundaries[index][1][i] = (color[1][i] - bgr[i])%256
            '''
            if self.boundaries[index][0][i] < 0:
                self.boundaries[index][0][i] = 255
            if self.boundaries[index][0][i] > 0:
               self.boundaries[index][0][i] = 255
            if self.boundaries[index][1][i] < 0:
                self.boundaries[index][1][i] = 255
            if self.boundaries[index][1][i] > 255:
               self.boundaries[index][1][i] = 0
            '''
            #print("adjust-",self.boundaries[index])




#conversion to HSV
hsv = RGB2HSV(img)

cv2.setMouseCallback('camera', mouse_clicked)
display_mode = ord('c')
depth_img=np.uint8(depth*(255/10000))

color_detected = [] #index 0 ... actual image , index 1 ... information 
for color in boundaries:
    lower = np.array(color[1], dtype = "uint8")
    upper = np.array(color[0], dtype = 'uint8')
    mask = cv2.inRange(img, lower, upper)
    output = cv2.bitwise_and(img, img, mask = mask)
    out = cv2.connectedComponentsWithStats( mask.astype(np.uint8))
    color_detected.append((output,out))

#print(color_detected)
BONES = Boundaries(boundaries)
index = 0
bgr = [0,0,0]
while 1:
    k = cv2.waitKey(200) & 0xFF
    tempk = k
    if tempk == ord('x'):
        k = display_mode
    if tempk == ord('r'):
        k = display_mode
    if tempk == ord('g'):
        k = display_mode
    if tempk == ord('b'):
        k = display_mode
    if k != 0xFF:
        display_mode = k

    if display_mode == ord('c'): # press c
        # display color image
        cv2.imshow('camera', img)
    elif display_mode == ord('d'): # press d
        # display depth map
        cv2.imshow('camera', depth_img)
    elif display_mode == ord('a'):
        
        cv2.imshow("images", np.hstack([img, color_detected[index][0]]))
    else:
        cv2.imshow('camera', img)
    
    if tempk == ord('x'):
        index += 1
        if index >= len(color_detected) or index < 0:
            index = 0
    if tempk == ord('r'):
        bgr[0] += 1
        BONES.adjust_bound(index,bgr)
        brg = [0,0,0]

    if tempk == ord('g'):
        bgr[1] += 1
        BONES.adjust_bound(index,bgr)
        brg = [0,0,0]

    if tempk == ord('b'):
        bgr[2] += 1
        BONES.adjust_bound(index,bgr)
        brg = [0,0,0]

    #print(BONES.boundaries[index])
    lower = np.array(BONES.boundaries[index][1], dtype = "uint8")
    upper = np.array(BONES.boundaries[index][0], dtype = 'uint8')
    mask = cv2.inRange(img, lower, upper)
    output = cv2.bitwise_and(img, img, mask = mask)
    out = cv2.connectedComponentsWithStats( mask.astype(np.uint8))
    color_detected[index] = ((output,out)) 
    area_cnts = cv2.findContours(mask.copy(),
                              cv2.RETR_EXTERNAL,
                              cv2.CHAIN_APPROX_SIMPLE)[-2]
    print("bluecnts_cnt:\n",len(area_cnts))
    if len(area_cnts)>0:
        cnt = 0
        for i in range(len(area_cnts)):
            area = area_cnts[i]
            (xg,yg,wg,hg) = cv2.boundingRect(area)
            print("surface:\n",color_detected[index][1][2][i+1][4])
            if color_detected[index][1][2][i+1][4] > 10: #na 4 pozice udajne sirka ale nejak nefunguje???????
                cnt += 1
                
                cv2.rectangle(color_detected[index][0],(xg,yg),(xg+wg, yg+hg),(0,255,0),2)#mozna chyba zde ale syntax je dle netu spravne????
    print("detection info:\n",color_detected[index][1][2])   


    # close window
    if k == ord('q'):
        break

cv2.destroyAllWindows()