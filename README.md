# TurtleBot
[TurtleBot 2](http://www.turtlebot.com/) is a platform designed for research and education.
The main part is the Kobuki base, which provide basic sensors (bumper, wheel drop, etc), digital and analog inputs, digital outputs and actuators.
In addition to the Kobuki sensors, the TurtleBot 2 has a Kinect-like RGBD sensor.

 - Link: [IEEE Spectrum: TurtleBot 2](http://spectrum.ieee.org/automaton/robotics/diy/turtlebot-2-now-available-for-preorder-from-clearpath-robotics)
 - Video: [Introducing Yujin Robot's Kobuki](https://www.youtube.com/watch?v=t-KTHkbUwrU)

We have two types of RGBD sensors, the first one is the Intel RealSense (turtle03-7) and the second is the Orbex Astra (turtle01-2).
Note that both the size of the image and its quality differ, the Astra has higher resolution and better quality.

## Using singularity container


- Start Ubuntu as singularity container:

```sh
$ singularity instance start /opt/ros ros
```

- Log into Ubuntu instance shell:

```sh
$ singularity shell instance://ros
```

- More info about singularity on [DCE Wiki](https://support.dce.felk.cvut.cz/mediawiki/index.php/singularity)

## Installation

- You can do this on the robot or on the lab workstation the home directories are the same.
- Be sure thet you are inside the Ubuntu container (see above).
- Source ROS environment:

```sh
$ source /opt/ros/kinetic/setup.bash
```

- Make directories for the ROS workspace:

```sh
$ mkdir -p ~/lar_ws/src
```

- Initialize ROS workspace:

```sh
$ cd ~/lar_ws/src
$ catkin_init_workspace
```

- Clone this repository:

```sh
$ cd ~/lar_ws/src
$ git clone https://gitlab.fel.cvut.cz/wagnelib/turtlebot.git
$ cd ~/lar_ws
$ catkin build
```

### Updating

- In order to update the workspace just update the turtle git repository

```sh
$ cd ~/lar_ws/src/turtlebot
$ git pull
```

- And build it inside the container

```sh
$ cd ~/lar_ws
$ catkin build
```

## Working with the TurtleBot

- Power up robot and onboard PC (NUC).
- **Note that switching off the robot will also cut power to the NUC.**
- After the boot, you should be able to log in (`ssh [username]@turtleXX`) using your lab login and password 
- If you like to open graphical windows on your desktop use `ssh -Y [username]@turleXX`
- Start singularity instance and enter the container (see [Using Singularity Container](using-singularity-containers))
- At this point it is wise to learn and start using terminal multiplexer such as [tmux](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/)
- Assuming that you have follower the [installation](instalation) instruction, everythong whould be ready.
- Source the LAR workspace, you can do this in each open terminal, or you can add it to the `~/.bashrc`.
- The robots are only visible from local network, so in order to use personal notebooks use cable or wifi (ssid: `e210bot` pass: `j6UsAC8a`)

```sh
$ source ~/lar_ws/devel/setup.bash
```

- Then you can start the robot, and camera drivers by calling

```sh
$ roslaunch turtlebot bringup_astra.launch          # for turtle01-02
$ roslaunch turtlebot bringup_realsense_R200.launch # for turtle03-07
$ roslaunch turtlebot bringup_realsense_D435.launch # for turtle08-13
```

- Then you can run your application in another terminal (thats why it is usefull to use `tmux`)

```sh
$ cd ~/lar_ws/src/turtlebot/scripts
$ python bumper_test.py
$ python example_move_1m.py
$ python random_walk.py
$ python record_data.py
$ python show_depth.py
$ python show_markers.py
$ python test_sensors.py
```

## TurtleBot package

The TurtleBot package provides means to interact with the robot. The package is located in the `src` directory of this repository and in installed on all Turtlebots.

### TurtleBot class

Initialization of the turtlebot:

``` python
from turtlebot import Turtlebot
turtle = Turtlebot()
```

#### `Turtlebot(rgb=False, depth=False, pc=False)`

Initializes Turtlebot object, the rgb, depth and pc arguments determine whether rgb images, depth maps and point clouds will be downloaded from the camera.

#### `cmd_velocity(linear=0, angular=0) -> None`

Commands velocity (m/s, rad/s) to the robot, in order to maintain the velocity this command must be called with rate of 10 Hz.

#### `get_odometry() -> [x,y,a]`

Returns odometry, i.e. estimation of the current position of the turtlebot, this position is references to the boot up position or to the last call of the `reset_odometry`. `[x,y]` are Cartesian coordinates of turtlebot in meters and `[a]` is the angle in Radians.

#### `reset_odometry() -> None`

Resets the odometry to `[0,0,0]` i.e. sets new starting position.

#### `get_rgb_image() -> image`

Returns RGB image as 480x640x3 Numpy array. In order to use RGB images initialize Turtlebot class with rgb stream i.e. `Turtlebot(rgb=True)`,

#### `get_depth_image() -> image`

Returns depth image as a 480x640x1 Numpy array, the depth is in meters. In order to use depth images initialize Turtlebot class with depth stream i.e. `Turtlebot(depth=True)`.

#### `get_point_cloud() -> point_cloud`

Returns point cloud in form of a 480x640x3 Numpy array. Values are in meters and in the coordinate system of the camera. In order to use point cloud initialize Turtlebot class with pc stream i.e. `Turtlebot(pc=True)`.

#### `play_sound(sound_id=0) -> None`

Plays one of predefined sounds: (0 - turn on, 1 - turn off, 2 - recharge start, 3 - press button, 4 - error sound, 5 - start cleaning, 6 - cleaning end).

#### `register_button_event_cb(fun) -> None`

Register callback for the button event, when the button is pressed the fun is called. The `fun` callback should have form of a function with one argument `button_cb(event)`, the `event` object has two variables `event.button` which stores the number of button that producing this event and `event.state` which stores the event type (0:released, 1:pressed). The `event` object is actually ROS message [kobuki_msgs/ButtonEvent](http://docs.ros.org/hydro/api/kobuki_msgs/html/msg/ButtonEvent.html).

#### `register_bumper_event_cb(fun) -> None`

Register callback for the bumper event, when the robot hits something the fun is called. The `fun` callback should have form of a function with one argument `bumper_cb(event)`, the `event` object has two variables `event.bumper` which stores the bumper number (0:left, 1:center, 2:right) and `event.state` which stores the event type (0:released, 1:pressed). The `event` object is actually ROS message [kobuki_msgs/BumperEvent](http://docs.ros.org/hydro/api/kobuki_msgs/html/msg/BumperEvent.html).  Do not hit anything!

#### `is_shutting_down(self) -> bool`

Returns true if CTRL+C was pressed. Use in the main loop as: 'while not turtle.is_shutting_down():'

#### `get_rgb_K(self) -> K`

Return K matrix (3x3 numpy array) for RGB camera.

#### `get_depth_K(self) -> K`

Return K matrix (3x3 numpy array) for depth camera.

### Marker Detector

#### `detect_markers(image) -> detections`

Detects markers in a image and returns detections as a list of tuples `(corners, id)`, where `corners` is list of points 4x2, and `id` is the identification number of detected marker.

#### `show_markers(image, detections)`

Draw detection into the image. See `show_marker.py` as an example.

### Tools

Some tools you might find helpful
#### keyop

In order to control the robot movements from command line, run the following command:

```
roslaunch kobuki_keyop safe_keyop.launch
```

#### tmux

Terminal multiplexer, one ssh connection multiple screens. Just run `tmux` in the terminal. `Ctrl+a c` creates new window `Ctrl+a N` where N is number of the window switches to that window. For more see [documentation](http://man.openbsd.org/OpenBSD-current/man1/tmux.1) but note that the prefix key is configured to be `Ctrl+b`.

#### Upload and download from turtlebot

In order to upload something to turtlebot one can use, several methods. One is to use `scp` as follows:

`scp -r local_folder ros@turtlebotXX:~/remote_folder`

on the other way round

`scp -r ros@turtlebotXX:~/remote_folder local_folder`

### Examples

Examples are stored in `scripts` in this repository.

The [show_depth.py](scripts/show_depth.py) demonstrates the point cloud acquisition and visualization.

The [show_markers.py](scripts/show_markers.py) demonstrates the acquisition of rgb images and detection of checkpoint markers.

The [random_walk.py](scripts/random_walk.py) demonstrate the use of depth sensor to avoid obstacles. The robot moves in straight line until it detects obstacle in the point cloud. Then it rotates until the obstacle disappear.

- [bumper_test.py](scripts/bumper_test.py) shows how to use `register_bumper_event_cb`, the `register_button_event_cb` is used in a similar way.

- [example_move_1m.py](scripts/example_move_1m.py)

- [test_sensors.py](scripts/test_sensors.py)


## Robotic Operating System

The software environment where the TurtleBot and sensors live is provided by Robotic Operating System (ROS). We heve made our best to hide the ROS part of the system from you so you can focus on the task. However, if interested you can found more information about ROS at [www.ros.org](http://www.ros.org/).

## Useful stuff

- In order to save array (depth image, points) one can use numpy's [save](https://docs.scipy.org/doc/numpy/reference/generated/numpy.save.html) function.

``` python
from turtlebot import Turtlebot
impot numpy as np

turtle = Turtlebot()
depth = turtle.get_depth_image()

np.save('depth.npy', depth)

# -----

depth = np.load('depth.npy')
```

### Using the on robot display

 - In order to use the onboard display one has to log on the robot using that display
 - Then in the terminal on the robot, through ssh is ok, but not in the singularity container run the following:

```sh
$ w
 13:22:47 up 21:07,  2 users,  load average: 0.23, 0.11, 0.03
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
username tty8     :1               13:17   21:07m 10.59s  0.15s xfce4-session
```
 - Then in the `FROM` section is the address of the display, for `username` the dispaly address is `:1`
 - Then in the terminal where you want to display someting on the robot display call the following:

```sh
$ export DISPLAY=:1
```

 - or try to source the [set-display](scritps/set-display) script, it should do the same as the above.

```sh
$ source ~/lar_ws/src/turtlebot/scripts/set-display
```

## Resources

- [FAQ](FAQ.md)
- [Presentation](presentation/turtlebot.pdf)
- [ROS: Robotic Operating System](http://ros.org)
- [ROS Wiki](http://wiki.ros.org)
